import { ipcRenderer } from "electron";

declare var externalAPI: any;

const notifyTrackInfo =
    () => ipcRenderer.send("notify-track", getCurrentTrackInfo());

function getCurrentTrackInfo() {
    const state: { [name: string]: string | any } = externalAPI.getCurrentTrack();
    return {
        album: state.album.title,
        artist: state.artists[0].title,
        icon: state.cover,
        like: state.liked,
        title: state.title,
        version: state.version,
        year: state.album.year,
    };
}

window.onload = () => {
    externalAPI.on(externalAPI.EVENT_TRACK, () => {
        notifyTrackInfo();
    });
};
