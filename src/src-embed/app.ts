import { ipcRenderer, WebviewTag } from "electron";

declare var notifyTrackInfo: any;
declare var isTrackLiked: boolean;

const webview = document.getElementById("webview") as WebviewTag;
const webviewExec = (code: string) => webview.executeJavaScript(code);

const ipcApiCalls: Array<[string, string]> = [
    ["media-next-track", "next"],
    ["media-prev-track", "prev"],
    ["media-stop", "togglePause"],
    ["media-play-pause", "togglePause"],
    /** Not implemented yet */
    ["dislike", "toggleDislike"],
];

const ipcApiCallsWithArg: Array<[string, string, string]> = [];

function bindIpcToApiCall(action: string, apiCall: string, apiCallArg: string = null) {
    const call = "externalAPI." + apiCall + "(" + apiCallArg  + ")";
    ipcRenderer.on(action, () => webviewExec(call));
}

ipcApiCalls.forEach((e, _, __) => bindIpcToApiCall(e["0"], e["1"]));
ipcApiCallsWithArg.forEach((e, _, __) => bindIpcToApiCall(e["0"], e["1"], e["2"]));
ipcRenderer.on("notify-track-changed", () => notifyTrackInfo());
ipcRenderer.on("devtools", () => webview.openDevTools());
ipcRenderer.on("like", () => {
    webviewExec(`var liked = externalAPI.getCurrentTrack().liked;
    if (!liked) { externalAPI.toggleLike() }`);
});
ipcRenderer.on("dislike", () => {
    webviewExec(`var disliked = externalAPI.getCurrentTrack().disliked;
    if (!disliked) { externalAPI.toggleDislike() }`);
});

const manageSidebar = (sidebarDisplay: string, centerblockWidth: string) => webviewExec(`
document.querySelector('.sidebar').style.display = '${sidebarDisplay}';
document.querySelector('.centerblock-wrapper').style.width = "${centerblockWidth}";
`);
const hideSidebar = () => manageSidebar("none", "100%");
const unhideSidebar = () => manageSidebar("", "auto");
const disableYandexLogo = () => webviewExec("document.querySelector('a.d-logo__ya').style.visibility = 'hidden';");
const disableFooter = () => webviewExec("document.querySelector('.footer').style.display = 'none'");
const expandPageToFitWindow = () => webviewExec(`
document.querySelector('html').style.overflowY = 'hidden';
document.querySelector('html').style.overflow = 'auto';
document.querySelector('body').style.overflowY = 'hidden';
document.querySelector('body').style.overflow = 'auto';
document.querySelector('html').style.width = '100%';
document.querySelector('html').style.height = '100%';
document.querySelector('body').style.width = '100%';
document.querySelector('body').style.height = '100%';
`);


webview.addEventListener("dom-ready", () => {
    hideSidebar();
    disableYandexLogo();
    expandPageToFitWindow();
    disableFooter();
});

