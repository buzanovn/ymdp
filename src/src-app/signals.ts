export enum Signals {
    NEXT = "media-next-track",
    PREV = "media-prev-track",
    STOP = "media-stop",
    PLAYPAUSE = "media-play-pause",
    LIKE = "like",
    DISLIKE = "dislike",
    DEVTOOLS = "devtools",
}
