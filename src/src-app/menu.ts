import { BrowserWindow, Menu } from "electron";
import { Signals } from "./signals";

const appMenuTemplate = (window: BrowserWindow) => [{
    label: "File",
    submenu: [{
        role: "quit",
    }],
}, {
    label: "Debug",
    submenu: [{
        label: "Open WebView Developers console",
        click() {  window.webContents.send(Signals.DEVTOOLS); },
    },
    {
        label: "Open application DevConsole",
        role: "toggledevtools",
    },
    {
        role: "reload",
    },
    ],
}];

const trayMenuTemplate = (window: BrowserWindow) => [
    {
        label:  "Like",
        click() { window.webContents.send(Signals.LIKE); },
    }, {
        label:  "Dislike",
        click() { window.webContents.send(Signals.DISLIKE); },
    }, {
        label:  "Next",
        click() { window.webContents.send(Signals.NEXT); },
    }, {
        label:  "Prev",
        click() { window.webContents.send(Signals.PREV); },
    }, {
        label: "Stop",
        click() { window.webContents.send(Signals.STOP); },
    }, {
        label: "Quit",
        click() { window.close(); },
  }];

class MenuCreator {
    public static createAppMenu(shouldCreate: boolean, window: BrowserWindow): Electron.Menu {
        return shouldCreate && Menu.buildFromTemplate(appMenuTemplate(window)) || null;
    }

    public static createTrayMenu(window: BrowserWindow): Electron.Menu {
        return Menu.buildFromTemplate(trayMenuTemplate(window));
    }
}

export = MenuCreator;
