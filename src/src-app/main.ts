import { app as electronApp, BrowserWindow, Menu, Tray } from "electron";
import * as path from "path";
import yargs = require("yargs");

import actionSignals = require("./actionsignal");
import MenuCreator = require("./menu");

function parseArgs(): yargs.Arguments {
    return yargs.options({
        debug: {
            alias: "d",
            default: false,
            demandOption: false,
            type: "boolean",
        },
        notifications: {
            alias: "n",
            default: false,
            demandOption: false,
            type: "boolean",
        },
    }).argv;
}

class App {

    public window: BrowserWindow = null;
    public tray: Tray = null;
    public shouldQuitApp: boolean = false;
    public debug: boolean;
    public notifications: boolean;
    private readonly indexPage: string = `file:///${__dirname}/index.html`;

    constructor(debug: boolean, notifications: boolean) {
        this.debug = debug;
        this.notifications = notifications;
        this.createWindow();
        this.setup();
        this.createTray();
    }

    private createTray(): void {
        this.tray = new Tray(getIcon(32));
        this.tray.setToolTip("Yandex Music Desktop Player");
        this.tray.setContextMenu(MenuCreator.createTrayMenu(this.window));
        this.tray.on("click", (e, b) => this.window.show());
    }

    private createWindow(): void {
        this.window = new BrowserWindow({ height: 700, icon: getIcon(128), width: 1080});
        this.window.webContents.on("new-window", (e, u, frN, d, windowOptions) => {
            windowOptions["node-integration"] = false;
        });
        this.window.on("close", (event: Event) => this.onClose(event))
            .on("minimize", (event: Event) => this.onMinimize(event))
            .on("closed", () => { this.window = null; });

        // TODO: to be implemented
        // window.setThumbarButtons([{
        //     tooltip: 'Play',
        //     icon: NativeImage.createFromPath(getIcon(48)),
        //     click() { window.webContents.send('MediaPlayPause') }
        // }]);
        Menu.setApplicationMenu(MenuCreator.createAppMenu(this.debug, this.window));
        this.window.loadURL(this.indexPage);
    }

    private onClose(event: Event): void {
        if (!this.shouldQuitApp) {
            event.preventDefault();
            switch (process.platform) {
                case "win32": case "linux":
                    this.window.close();
                    break;
                case "darwin":
                    this.window.hide();
                    break;
            }
        }
    }

    private onMinimize(event: Event): void {
        event.preventDefault();
        this.window.hide();
    }

    private bindShortcuts(): void {
        actionSignals.forEach((actionSignal) => actionSignal.bind(this.window));
    }

    private setup(): void {
        this.bindShortcuts();
    }
}

const args = parseArgs();
let app: App = null;

electronApp.on("ready", () => {
    app = new App(args.debug, args.notifications);
});
electronApp.on("before-quit", () => { app.shouldQuitApp = true; });
electronApp.on("activate", (_) => app.window.show());

function getIcon(size: number): string {
    return path.join(__dirname, `../icons/source_colored_png/${size}x${size}.png`);
}
