import {BrowserWindow, globalShortcut} from "electron";
import { Signals } from "./signals";

class ActionSignal {
    constructor(readonly action: string, readonly signal: string) {}
    public bind(window: BrowserWindow) {
        globalShortcut.register(this.action, () => window.webContents.send(this.signal));
    }
}

const actionSignals: ActionSignal[] = [
    new ActionSignal("MediaNextTrack", Signals.NEXT),
    new ActionSignal("MediaPreviousTrack", Signals.PREV),
    new ActionSignal("MediaStop", Signals.STOP),
    new ActionSignal("MediaPlayPause", Signals.PLAYPAUSE),
];

export = actionSignals;
