# Yandex Music Desktop Client
A simple dektop client, developed as an Electron wrapped web application, that provides some nice native functions using Yandex Music API (externalAPI in webpage)

## Build
Clone repository
``` 
    git clone https://buzanovn@bitbucket.org/buzanovn/ymdp.git
```

Install all dependecies described in `package.json` (be sure you are in repository root folder)
```
    npm i 
```

Then use gulp to build the application for desired platfrom. This will automaticaly transpile Typescript and run electron-packager with described in `gulpfile.babel.js` parameters (Windows version is under maintenance)

```
    gulp build-{linux,darwin,win}
```

## Download assemblies
Existing versions of applications

- **2.0.0-1**
    - [ymdp-linux-x64-2.0.0-1.tar.gz](https://yadi.sk/d/WCGd-WcE3aM2wb)
    - [ymdp-darwin-x64-2.0.0-1.tar.gz](https://yadi.sk/d/6-XJfZ3k3aLZGu)
- 2.0.0
    - [ymdp-linux-x64-2.0.0.tar.gz](https://yadi.sk/d/fa0UO3ae3aM2uC)
    - [ymdp-darwin-x64-2.0.0.tar.gz](https://yadi.sk/d/ZDsqTp363aM2un)


