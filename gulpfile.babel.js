import gulp from 'gulp'
import jedi from 'gulp-json-editor'

import del from 'del'
import targz from 'targz'
import git from 'git-rev-sync'
import electronPackager from 'electron-packager'
import copyNodeModules from 'copy-node-modules'
import path from 'path'
import fs from 'fs'
import { YandexDisk } from 'yandex-disk'
import semver from 'semver'

import { createTasksGroup } from './.gulp/variants'
import { ginfo, gerr, gwarn} from './.gulp/gulp-log'

import * as _p from './package.json'
let p = _p

const stagingDir = '.staging_build_dir'
const buildDir = 'build'
const electronVersion = '2.0.7'
const yandexCredentials = () => {
    return JSON.parse(fs.readFileSync('.yandex-creds.json'))
}

const buildOptions = (variant) => {
    return {
        dir: stagingDir,
        arch: variant.arch,
        electronVersion: electronVersion,
        icon: variant.arch,
        out: buildDir,
        name: p.name,
        platform: variant.platform,
        prune: true,
        overwrite: true,
        quiet: true
    }
}

gulp.task('incv', () => incv('prerelease'))

gulp.task('clean-build', (done) => del([buildDir]))
gulp.task('clean-staging', (done) => del([stagingDir]))
gulp.task('clean', gulp.parallel('clean-build', 'clean-staging'))

gulp.task('copy-node-modules', (done) => { copyNodeNonDevModules(done) })
gulp.task('copy', gulp.series('clean-staging', 'copy-node-modules', copyApp))

createTasksGroup('build', 'copy', '', build)
createTasksGroup('assemble', '', 'build', assemble)
createTasksGroup('publish', '', 'assemble', publish)

gulp.task('default', () => {})

function incv(inctype) {
    return gulp.src('./package.json').pipe(jedi((json) => {
        json.version = semver.inc(json.version, inctype)
        return json
    })).pipe(gulp.dest('./', {overwrite: true}))
}

function copyNodeNonDevModules (onDone) {
    return copyNodeModules('.', stagingDir, {devDependencies: false}, (err, _) => {
        if (err) {
            throw err
        } else {
            return onDone()
        }
    })
}

function copyApp () {
    return gulp.src(['./app/**/*', './icons/**/*', './package.json'], {'base': '.'})
        .pipe(gulp.dest(stagingDir))
}

function build (variant, callback) {
    ginfo(`Buidling ${variant.platform}-${variant.arch}`)
    return electronPackager(buildOptions(variant), (err) => {
        if (err) {
            callback()
        } else {
            callback()
        }
    })
}

const assemblyName = (v) => `${p.name}-${v.platform}-${v.arch}-v${p.version}-${git.short()}`

function assemble (variant, callback) {
    let builtPath = path.join(buildDir, `${p.name}-${variant.platform}-${variant.arch}/`)
    fs.exists(builtPath, (exists) => {
        if (exists) {
            targz.compress({
                src: builtPath,
                dest: path.join(buildDir, `${assemblyName(variant)}.tar.gz`)
            }, (err) => {
                if (err) {
                    gwarn('Failed to create archive')
                    throw err
                } else {
                    return callback()
                }
            })
        } else {
            gwarn('Built folder does not exist', 'Silently proceeding to the next assebmle (if it should be done)')
            callback()
        }
    })
}

function publish (variant, callback) {
    let filename = assemblyName(variant) + '.tar.gz'
    let filepath = path.join(buildDir, filename)
    fs.exists(filepath, (exists) => {
        if (exists) {
            upload(filepath, filename, callback)
        } else {
            gwarn('Assembled app does not exist', 'Silently proceeding to the next publish (if it should be done)')
            callback()
        }
    })
}

function upload(fp, fn, callback) {
    let creds = yandexCredentials()
    let disk = new YandexDisk(creds.login, creds.password)
    ginfo('Logined into YandexDisk')
    disk.mkdir('/ymdp', (err) => {
        if (err) {
            gerr('Something went wrong while making dir')
            throw err
        }
        ginfo('The folder was created, starting to upload file')
        disk.uploadFile(fp, path.join('/ymdp', fn), (err) => {
            if (err) {
                gerr('Something went wrong while uploading file')
                throw err
            } else {
                ginfo('File was uploaded')
                callback()
            }
        })
    })
}
