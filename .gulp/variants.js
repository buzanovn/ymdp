import gulp from 'gulp'

import * as variants from './variants.json'

const variantKeys = Object.keys(variants).filter((v) => v !== 'default')

function iter(cb) {
    return variantKeys.forEach((name, _, __) => {
        let variant = variants[name]
        return cb(name, variant)
    })
}

export function createTasksGroup(familyName, commonDepTask, partDepTask, function_) {
    // console.log("/n/n/n", familyName, commonDepTask, partDepTask, function_)
    iter((name, props) => {
        // console.log(name, props)
        let depTask = ''
        if(commonDepTask !== '') {
            depTask = commonDepTask
        } else{
            depTask = props[partDepTask]
        }
        // console.log("DTP", depTask, props, familyName)
        gulp.task(props[familyName], gulp.series(depTask, (callback) => { function_(props, callback) }))
    })
    gulp.task(familyName, gulp.series.apply(this, variantKeys.map((name) => variants[name][familyName])))
}

export default createTasksGroup