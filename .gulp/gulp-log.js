import gutil from "gulp-util"

function _log(type, color, message, additionalInfo) {
    var additionalString = additionalInfo ? additionalInfo.join(' ') : ""
    gutil.log(color(type + ': ' + message), additionalString)
}

export function ginfo(message) { _log("Info", gutil.colors.blue, message) }
export function gwarn(cause, ...additionalInfo) { _log("Warning", gutil.colors.yellow, cause, additionalInfo) }
export function gerr(cause, ...additionalInfo) { _log("Error", gutil.colors.red, cause, additionalInfo) }

export class GLog {
    constructor() {
        this.ginfo = ginfo
        this.gwarn = gwarn
        this.gerr = gerr
    }
}

export default GLog